import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import './sass/style.scss';

ReactDOM.render(<BrowserRouter basename="/"><App/></BrowserRouter>, document.getElementById('root'));