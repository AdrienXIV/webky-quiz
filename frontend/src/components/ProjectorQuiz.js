import React from 'react';
import { Progress, Image } from 'semantic-ui-react'
import $ from 'jquery';
import { Ranking } from './Ranking';

export class ProjectorQuiz extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            seconde: 30,
            time: 30, // temps de référence
            pseudo: "",
            text: "",
            room: this.props.room,
            numbers_players: 0,
            response: false,
            redirect: false,
            media: false, // image/video des questions
            media_type: 'image',
            media_url: '', // media avant
            media_after: '',
            replies: [],
            isRankingShowed: false,
            ranking_array: [],
            socket: this.props.socket
        };
        this.chrono_interval = setInterval(() => { }, 100);
    };

    componentDidMount() {
        const { socket, room } = this.state;
        socket.emit('projector', room);

        socket.on('players', response => {
            let text = "";
            response.players.forEach(elem => {
                text += "<p style='font-size:xx-large'>" + elem.pseudo + "</p>";
            });
            this.setState({
                numbers_players: response.number,
                text: text
            });
        });

        socket.on('show_question', response => {
            this.setState({
                seconde: response.time / 1000,
                time: response.time / 1000,
                media: false, // remise à 0
                replies: response.replies,
                isRankingShowed: false
            });

            //$('#quiz-image').hide(); // enlever l'image du début

            $('#chrono').css('display', 'flex !important').fadeIn(2000);
            $('#question').html(''); // remise à 0
            $('#replies').html(''); // remise à 0

            let question = response.question;
            if (question.image_before != '') {
                this.setState({
                    media: true,
                    media_type: 'image',
                    media_url: question.image_before,
                    media_after: question.image_after
                });
            } else if (question.youtube_before != '') {
                this.setState({
                    media: true,
                    media_type: 'video',
                    media_url: question.youtube_before,
                    media_after: question.youtube_after
                });
            }

            let content = '<div className="column" data-id="' + question.id + '" data-order="' + question.order + '">' + question.name + '</div>';
            $('#question').html(content);
        });

        socket.on('show_reply', response => {
            let index = response.reply_number;
            let content = '<div className="column" id="' + this.state.replies[index].id + '">' + this.state.replies[index].name + '</div>';
            $('#replies').append(content);

        });

        socket.on('show_good_reply', response => {
            if (this.state.media) {
                this.setState({
                    media_url: this.state.media_after // affichage du média confirmant la bonne réponse
                });
            }
            $('#' + response).css({
                'background-color': '#009432',
                'border': '1px solid #009432',
                'color': 'white'
            });
        });

        socket.on('end_chrono', () => {
            this.endChrono();
        });

        socket.on('end_quiz', response => {
            if (response === 1) {
                $('#chrono').hide();
                $('#question').html('');
                $('#replies').html('');
                $('#end_quiz').fadeIn(2000)

                //TODO: afficher classement final
            }
        });

        socket.on('start_chrono', time => {
            $('#valider').show();
            this.chrono(time);
        });
        socket.on('end_chrono', () => {
            this.endChrono();
        });

        socket.on('projector_show_ranking', () => {
            this.setState({
                isRankingShowed: true
            });
        });

        socket.on('play_video', response => {
            var video = document.getElementById("myVideo");
            switch (response) {
                case 0:
                    this.pauseVideo(video);
                    break;
                case 1:
                    this.playVideo(video);
                    break;
                case 10:
                    this.replayVideo(video);
                    break;

                default:
                    break;
            }
        });
    }

    chrono(time) {
        $('#valider button').removeClass('disabled');
        $('#valider').show();

        this.setState({
            seconde: (time / 1000)
        });

        let value = this.state.seconde;
        this.chrono_interval = setInterval(() => {
            this.setState({
                seconde: (value - 0.1).toFixed(2)
            });
            if (Number(this.state.seconde) === 0) {
                clearInterval(this.chrono_interval);
                $('#valider button').addClass('disabled');
                $('#valider').hide();
            }
            else {
                value = this.state.seconde;
            }
        }, 100);
    }

    endChrono() {
        this.setState({
            seconde: 0 // si y'a décallage
        });
        clearInterval(this.chrono_interval);
    }

    showMedia() {
        if (this.state.media) {
            if (this.state.media_type === 'image') {
                return <Image src={__dirname + "images/" + this.state.media_url} rounded centered style={{ marginTop: '1%', height: '35%' }} />
            } else {
                return (
                    <video controls heigth="35%" id="myVideo" key={__dirname + "videos/" + this.state.media_url}>
                        <source src={__dirname + "videos/" + this.state.media_url} type="video/mp4" />
                    </video>
                );
            }
        }
    }
    playVideo(idVideo) {
        idVideo.play();
    }
    pauseVideo(idVideo) {
        idVideo.pause();
    }
    replayVideo(idVideo) {
        idVideo.load();
    }

    render() {
        /*
          <Image size="huge" src={"https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg"} rounded centered style={{marginTop:'1%'}} />
          */
        if (this.state.isRankingShowed) {
            return <Ranking projector={true} socket={this.state.socket} room={this.state.room} />
        } else {
            return (
                <div id="ProjectorQuiz" className="ui fluid container" style={{ height: '100vh', position: 'absolute' }}>
                    {this.showMedia()}

                    <div id="chrono">
                        <Progress percent={this.state.seconde * (100 / this.state.time)} indicating label={this.state.seconde + " s"} size="large" />
                    </div>
                    <div className="ui two column centered grid" id="question">

                    </div>
                    <div className="ui stackable height column grid" id="replies"></div>

                    <div id="end_quiz">Fin du QUIZ !</div>
                    <div className="ui height column centered grid quiz-question-reponses"></div>
                </div>
            );
        }
    }
}