import React from 'react';
import ReactHtmlParser from 'react-html-parser';
import { ProjectorQuiz } from './ProjectorQuiz';
import $ from 'jquery';

export class Projector extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pseudo: "",
            text: "",
            url: "https://mateo.webky.fr/" + this.props.match.params.id, //texte sous le qrcode
            room: this.props.match.params.id,
            numbers_players: 0,
            response: false,
            redirect: false,
            socket: this.props.socket
        };
    };

    componentDidMount() {
        const { socket, room } = this.state;

        $('#qrcode').show();
        $('#quiz-image').hide();

        socket.emit('projector', room);

        socket.on('players', response => {
            let text = "";
            response.players.forEach(elem => {
                text += "<p>" + elem.pseudo + "</p>";
            });
            this.setState({
                numbers_players: response.number,
                text: text
            });
        });

        socket.on('start', () => {
            this.setState({
                redirect: true
            });
        });

        socket.on('players', response => {
            let text = "";
            response.players.forEach(elem => {
                text += "<p>" + elem.pseudo + "</p>";
            });
            this.setState({
                numbers_players: response.number,
                text: text
            });
        });
    }


    render() {
        if (this.state.redirect) {
            return <ProjectorQuiz room={this.state.room} socket={this.state.socket} />;
        } else {
            return <div id="videoProj" className="ui two column grid">
                <div className="column" id='first-column'>
                    <div className="ui segment" id="qrcode">
                        <img src={"../qrcode/" + this.props.match.params.id + ".svg"} id="image-qrcode" alt="" />
                        <div>{this.state.url}</div>
                    </div>
                </div>
                <div className="column" id="wait-admin">
                    <div className="ui segment">
                        <div>
                            <h5 className="ui header">Joueurs connectés</h5>
                            <h2 className="ui center aligned icon header">
                                <i className="circular users icon"></i>
                                {this.state.numbers_players}
                            </h2>
                            <div id="players-connected" className="ui raised very padded text ">
                                {ReactHtmlParser(this.state.text)}
                            </div>
                            <div id="loader">
                                <div className="ui active inline loader"></div>
                                <small>En attente de l'administrateur.</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        }
    }

}