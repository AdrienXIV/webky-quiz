import React from 'react';

export class Reply extends React.Component {
    constructor(props) {
        super();
    }

    render() {
        return <div className="column ui button" onClick={this.props.onClick} id={this.props.id}>{this.props.name}</div>;
    }
} 
