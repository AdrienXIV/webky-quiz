import React from 'react';
import { Redirect } from 'react-router-dom'

export class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pseudo: "",
            error: "",
            room: this.props.match.params.id,
            redirect: false,
            socket: this.props.socket
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    };

    componentDidMount() {
        const { socket } = this.state;
        // session vierge
        sessionStorage.clear('user');
        sessionStorage.clear('game_connected');

        socket.on("socket_id", id => {
            console.log("socket id : " + id);
            this.setState({
                redirect: true
            });
        })
    }

    handleChange = (e) => {
        this.setState({
            pseudo: e.target.value,
            error: ""
        });
    }

    handleClick = () => {
        const { pseudo, socket, room } = this.state;

        sessionStorage.setItem('user', pseudo);

        socket.emit('login', {
            room,
            pseudo
        });
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to={'/' + this.state.room + '/quiz'} />
        }
        else {
            return (
                <div id="login">
                    <div className="ui left icon input">
                        <input type="text" value={this.state.pseudo} onChange={this.handleChange} placeholder="Pseudo" />
                        <i className="users icon"></i>
                        <button type="button" className="ui green basic button" onClick={this.handleClick}>Valider</button>
                    </div>
                </div>
            );
        }
    }
}