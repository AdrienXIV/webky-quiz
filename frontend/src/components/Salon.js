import React from 'react';
import ReactHtmlParser from 'react-html-parser';
import { WaitingAdmin } from './WaitingAdmin';

export class Salon extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pseudo: sessionStorage.getItem('user') === null ? '' : sessionStorage.getItem('user'),
            game_connected: sessionStorage.getItem('game_connected') === null ? false : sessionStorage.getItem('game_connected'),
            text: "",
            room: this.props.match.params.id,
            numbers_players: 0,
            redirect: false,
            connected: false,
            socket: this.props.socket
        };
    };


    componentDidMount() {
        const { socket, room, pseudo } = this.state;
        this.interval_socketConnected();

        if (pseudo === '') {
            this.setState({
                redirect: true,
                path: 'login'
            });
        }

        this.setState({
            text: ""
        });

        /*if (!connected) {
            socket.emit("login", {
                room,
                pseudo
            }).addEventListener('user_session', response => {
                
                sessionStorage.setItem('socket_id', response);
            });
        }*/

        socket.emit('game_info', room).addEventListener('players', response => {
            let text = "";
            response.players.forEach(elem => {
                if (elem.pseudo === this.state.pseudo) {
                    text += "<p style='background-color:#f1f2f6;'>" + elem.pseudo + "</p>";
                } else {
                    text += "<p>" + elem.pseudo + "</p>";
                }
            });
            this.setState({
                numbers_players: response.number,
                game_connected: true,
                text: text,
                connected: true
            });
        });


        socket.on('game_id', id => {
            sessionStorage.setItem('game_id', id);
        });
        socket.on('start', response => {
            // être lié à la partie 
            sessionStorage.setItem('game_connected', true);

            console.log(response)
            if (response === 1) {
                this.setState({
                    redirect: true
                });
            }
        });
    }

    interval_socketConnected() {
        const { socket } = this.state;
        setInterval(() => {
            if (socket.disconnected) {
                console.log('disconnected');
                this.setState({
                    connected: false
                });
            } else if (socket.connected) {
                console.log('connected')
            }
        }, 5000);
    }

    render() {
        if (this.state.redirect) {
            return <WaitingAdmin room={this.state.room} socket={this.state.socket} numbers_players={this.state.numbers_players} />
        }
        else {
            return <div id="salon">
                <h5 className="ui header">Joueurs connectés</h5>
                <h2 className="ui center aligned icon header">
                    <i className="circular users icon"></i>
                    {this.state.numbers_players}
                </h2>
                <div id="players-connected" className="ui raised very padded text container segment">
                    {ReactHtmlParser(this.state.text)}
                </div>
                <div id="loader">
                    <div className="ui active inline loader"></div>
                    <small>En attente de l'administrateur.</small>
                </div>
            </div>
        }
    }
}