import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';
import { Image } from 'semantic-ui-react'

export class Ranking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            "images_rank": [
                "https://vignette.wikia.nocookie.net/mariokart/images/6/6e/MKHF_Number_1_Icon.png/revision/latest?cb=20190219120032",
                'https://vignette.wikia.nocookie.net/mariokart/images/c/ca/MKHF_Number_2_Icon.png/revision/latest/top-crop/width/300/height/300?cb=20190219120033',
                'https://vignette.wikia.nocookie.net/mariokart/images/6/62/3rd_Icon_-_Super_Koopa_Kart.png/revision/latest/top-crop/width/220/height/220?cb=20180404093626',
                'https://vignette.wikia.nocookie.net/mariokart/images/3/33/MKHF_Number_4_Icon.png/revision/latest/top-crop/width/300/height/300?cb=20190219120033',
                'https://vignette.wikia.nocookie.net/mariokart/images/0/0d/5th_Icon_-_Super_Koopa_Kart.png/revision/latest?cb=20180404093416'
            ],
            content: '',
            socket: this.props.socket,
            room: this.props.room
        }
    }

    componentDidMount() {
        const { socket } = this.state;

        socket.emit('ranking', {
            room: this.state.room,
            game_id: Number(sessionStorage.getItem('game_id'))
        });

        socket.on('show_ranking', response => {
            let content = '';
            
            response.sort(function (a, b) {
                return b.score - a.score;
            });

            content += '<div class="ui middle aligned list">';
            for (let i = 0; i < response.length; i++) {
                // affichage des 5 premiers pour le projecteur
                if (this.props.projector) {
                    content += '<div class="item" id="' + response[i].id + '">';
                    content += '<div class="right floated content">';
                    content += '<a class="ui blue circular label">' + response[i].score + '</a>';
                    content += '</div>';
                    content += '<img class="ui avatar image" src="' + this.state.images_rank[i] + '" style="border-radius: 0"/>';
                    content += '<div class="middle aligned content" style="margin-left:5%">';
                    content += response[i].player
                    content += '</div>';
                    content += '</div>';
                    if (i === 4) break;
                } else {
                    // affichage de tous pour les joueurs
                    if(response[i].player === sessionStorage.getItem('user')){
                        content += '<div class="item" id="' + response[i].id + '" style="background-color: #ecf0f1">';
                    }else{
                        content += '<div class="item" id="' + response[i].id + '">';
                    }
                    content += '<div class="right floated content">';
                    content += '<div class="ui">' + response[i].score + '</div>';
                    content += '</div>';
                    content += '<div class="left floated content">';
                    content += '<a class="ui black circular label">' + (i + 1) + '</a>'; // compteur démarre à 0
                    content += '</div>';
                    content += '<div class="middle aligned content">';
                    content += response[i].player;
                    content += '</div>';
                    content += '</div>';
                }
            }
            content += '</div>';
            this.setState({ content: content });
        });
    }

    render() {
        return (
            <div>
                <Image id="ranking-image" src={"https://montreagence.com/wp-content/uploads/eric-bourdon-lauriers.png"} size="large" centered />
                {ReactHtmlParser(this.state.content)}
            </div>
        );
    }
}