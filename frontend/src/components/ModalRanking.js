import React, { Component } from 'react';
import { Modal } from 'semantic-ui-react';
import { Ranking } from './Ranking';


export class ModalRanking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            socket: this.props.socket,
            room: this.props.room
        }
    }

    render() {
        return (
            <Modal trigger={this.props.trigger}>
                <Modal.Header>Classement</Modal.Header>
                <Modal.Content scrolling>
                    <Modal.Description>
                        <Ranking projector={false} socket={this.state.socket} room={this.state.room} />
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        );
    }
}