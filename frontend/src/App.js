import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { Login } from "./components/Login";
import { Salon } from "./components/Salon";
import { Projector } from "./components/Projector";
import io from 'socket.io-client';

/**
 * INFOS SOCKET.IO
 */
const __URL = "https://mateo.webky.fr";
const __OPTIONS = { secure: true };
//

const Image = () => {
  return <div id="quiz-image"><img src="https://media1.giphy.com/media/nUfZ8ne8voiLC/source.gif" alt="quiz" /></div>;
}

class App extends Component {
  constructor(props) {
    super();
    this.state = {
      room: "",
      redirect: false,
      socket: io(__URL, __OPTIONS)
    };
  };

  componentDidMount() {
    const { socket } = this.state;
    socket.on('projector_room', response => {
      this.setState({
        room: response,
        redirect: true
      });
    });
  }

  renderRedirect() {
    if (this.state.redirect) {
      return <Redirect to={'/projector/' + this.state.room} ></Redirect>;
    }
  }
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Image} />
        <Route exact path="/projector" component={Image} />
        <Route exact path="/projector/:id" render={(props) => <Projector {...props} socket={this.state.socket} />} />
        <Route exact path="/:id" render={(props) => <Login {...props} socket={this.state.socket} />} />
        <Route exact path="/:id/quiz" render={(props) => <Salon {...props} socket={this.state.socket} />} />
      </Switch>
    );
  }
}
export default App;