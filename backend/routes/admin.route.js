var router = require('express').Router();
var Question = require('../models/Question');
const QuestionCollection = require('../models/QuestionCollection');
const ReplyCollection = require('../models/ReplyCollection');

var randomstring = require("randomstring");
var fs = require('fs');
var qr = require('qr-image');

/**
 * CONSTANTES
 */
const _URL = 'https://mateo.webky.fr/';
const _URL_PROJECTOR = 'https://mateo.webky.fr/projector/';


var Quiz = require('../models/Quiz');
var Reply = require('../models/Reply');
var Promise = require('bluebird');

/**
 * INFO CONNEXON
 */
const USERNAME = 'webky';
const PASSWORD = 'XBE0ZHFQ1XgVgEb6';


/**
 * GET
 */

// récupérer formulaire connexion
router.get('/login', function (req, res, next) {
    if (req.session.admin) next();
    else {
        res.render('admin/login.ejs', {
            username: '',
            error: ''
        });
    }
});

// redirection formulaire connexion si non connecté
router.get('*', function (req, res, next) {
    if (!req.session.admin) {
        res.redirect('/admin/login');
    } else {
        next();
    }
});


// récupérer tous les quiz
router.get('/', function (req, res) {
    Quiz.query(function (qb) {
        qb.orderBy('id', 'DESC')
    }).fetchAll().then(function (collection) {
        let posts = collection.serialize();

        res.render('admin/index.ejs', {
            posts
        });
    }).catch(err => {
        console.error(err);
    })
});

router.get('/logout', function (req, res) {
    req.session.destroy(function (err) {
        if (err) console.error(err);
        else {
            res.redirect('/admin/login');
        }
    })
});

// vue pour ajouter quiz
router.get('/add', function (req, res) {
    res.render('admin/quiz-add-edit.ejs', {
        quiz: '',
        questions: [],
        method: 'POST',
        url: '/admin/edit',
        delete_button: 'disabled'
    });
});

// vue pour modifier quiz
router.get('/edit/:id', function (req, res) {
    let quiz = {};
    let questions = [];

    Quiz.where('id', req.params.id).fetch()
        .then(model => {
            quiz = model.serialize();

            return Question.query(function (qb) {
                qb.orderBy('order', 'ASC')
            }).where('quiz_id', req.params.id).fetchAll({ withRelated: ['reply'] })
        })
        .then(collection => {
            questions = collection.serialize();

            res.render('admin/quiz-add-edit.ejs', {
                quiz,
                questions,
                method: 'PATCH',
                url: '/admin/' + quiz.id,
                delete_button: ''
            });
        })
        .catch(err => {
            console.error(err);
        });
});


// vue pour jouer quiz
router.get('/play/:id', function (req, res) {
    let quiz = {};

    Quiz.where('id', req.params.id).fetch()
        .then(model => {
            quiz = model.serialize();

            return Question.query(function (qb) {
                qb.orderBy('order', 'ASC')
            }).where('quiz_id', quiz.id).fetchAll({ withRelated: 'reply' })
        })
        .then(collection => {
            let questions = collection.serialize();

            res.render('admin/play.ejs', {
                quiz,
                questions
            });
        })
        .catch(err => console.error(err));
});

// QR code d'un quiz
router.get('/play/:id/start', function (req, res) {
    let quiz = {};

    Quiz.where('id', req.params.id).fetch()
        .then(model => {
            quiz = model.serialize();

            // url du QR code pour les joueurs
            let code = quiz.url;

            let qr_code = qr.imageSync(String(_URL + code), { type: 'svg' });

            let qr_code_file_name = code + '.svg';

            fs.writeFileSync('./public/qrcode/' + qr_code_file_name, qr_code, (err) => {
                if (err) {
                    console.log(err);
                }
            });
            res.redirect(_URL_PROJECTOR + quiz.url);
        })
        .catch(err => {
            console.error(err);
        });
});


/**
 * POST
 */

// formulaire connexion
router.post('/login', function (req, res) {

    if (req.body.username == USERNAME) {
        if (req.body.password == PASSWORD) {
            req.session.regenerate(function (err) {
                if (err) console.error(err);
                else {
                    req.session.cookie.path = '/admin';
                    req.session.admin = {
                        'id': req.session.id,
                        'username': req.body.username
                    };
                    res.redirect('/admin');
                }
            });
        } else {
            res.render('admin/login.ejs', {
                username: req.body.username,
                error: 'Mot de passe incorrect !'
            });
        }
    } else {
        res.render('admin/login.ejs', {
            username: "",
            error: 'Identifiant et/ou mot de passe incorrect !'
        });
    }
});

// création d'un quiz
router.post('/edit', function (req, res) {
    let table = req.body.table;
    let question = [];
    let reponse = [];
    console.log(table)

    let url = randomstring.generate(5);

    // s'il n'y a pas de questions
    if (table == undefined) {
        Quiz.forge({
            name: req.body.name,
            description: req.body.description,
            image: req.body.image,
            url
        }).save().then(() => {
            res.send('ok');
        }).catch(err => {
            console.error(err);
        })
    } else {
        Quiz.forge({
            name: req.body.name,
            description: req.body.description,
            image: req.body.image,
            url
        }).save()
            .then(model => {
                table.forEach(element => {
                    question.push({
                        order: element.question.order,
                        name: element.question.name,
                        image_before: element.question.image_before,
                        image_after: element.question.image_after,
                        youtube_before: element.question.youtube_before,
                        youtube_after: element.question.youtube_after,
                        quiz_id: model.get('id')
                    });
                });
                let questions = QuestionCollection.forge(question);
                return Promise.all(questions.invokeMap('save'))
            })
            .then(collection => {
                let collection_id = [];

                collection.forEach(model => {
                    collection_id.push({ question_id: model.attributes.id })
                });

                /**
                 * index de l'id de la question. 
                 * exemple: 2 questions = 2 id 
                 * donc 4 réponses avec chaque id
                 * reponse 1, 2, 3, 4, id de l'index [0] etc
                 */
                let i = 0;
                table.forEach(element => {
                    element.reponses.forEach(rep => {
                        reponse.push({
                            name: rep.name,
                            is_correct: rep.is_correct,
                            question_id: collection_id[i].question_id
                        });
                    });
                    i++;
                });
                let reponses = ReplyCollection.forge(reponse);
                return Promise.all(reponses.invokeMap('save'))
            })
            .then(() => {
                res.send('ok');
            })
            .catch(err => {
                console.error(err);
            });
    }
});

router.post('/file', function (req, res) {

})


/**
 * PATCH
 */

// modification d'un quiz
router.patch("/:id", function (req, res) {
    let table = req.body.table;
    let question = [];
    let reponse = [];

    // s'il n'y a pas de questions
    if (table == undefined) {
        Quiz.where('id', req.params.id).fetch()
            .then(model => {
                model.set({
                    name: req.body.name,
                    description: req.body.description,
                    image: req.body.image
                });
                return model.save()
            })
            .then(() => {
                res.send('ok');
            })
            .catch(err => {
                console.err(err);
            })
    } else {
        Quiz.where('id', req.params.id).fetch({ require: true })
            .then(model => {
                return Question.where('quiz_id', model.id).fetchAll()
            })
            .then(collection => {
                return collection.invokeThen("destroy")
            })
            .then(() => {
                return Quiz.where('id', req.params.id).fetch({ require: true })
            })
            .then((model) => {
                model.set({
                    name: req.body.name,
                    description: req.body.description,
                    image: req.body.image
                });
                return model.save()
            })
            .then(model => {
                table.forEach(element => {
                    question.push({
                        order: element.question.order,
                        name: element.question.name,
                        image_before: element.question.image_before,
                        image_after: element.question.image_after,
                        youtube_before: element.question.youtube_before,
                        youtube_after: element.question.youtube_after,
                        quiz_id: model.id
                    });
                });
                let questions = QuestionCollection.forge(question);
                return Promise.all(questions.invokeMap('save'))
            }).then(collection => {
                let collection_id = [];
                collection.forEach(model => {
                    collection_id.push({ question_id: model.attributes.id });
                });

                /**
                * index de l'id de la question.
                * exemple: 2 questions = 2 id
                * donc 4 réponses avec chaque id
                * reponse 1, 2, 3, 4, id de l'index [0] etc
                */
                let i = 0;
                table.forEach(element => {
                    element.reponses.forEach(rep => {
                        reponse.push({
                            name: rep.name,
                            is_correct: rep.is_correct,
                            question_id: collection_id[i].question_id
                        });
                    });

                    i++;
                });
                let reponses = ReplyCollection.forge(reponse);
                return Promise.all(reponses.invokeMap('save'))
            }).then(() => {
                res.send('ok');
            })
            .catch(err => {
                console.error(err);
            })
    }
});


/**
 * DELETE
 */

// Suppression d'un quiz
router.delete("/:id", function (req, res) {
    Quiz.where('id', req.params.id).fetch({ require: true })
        .then(model => {
            return Question.where('quiz_id', model.id).fetchAll()
        })
        .then(collection => {
            return collection.invokeThen("destroy")
        })
        .then(() => {
            return Quiz.where('id', req.params.id).fetch({ require: true })
        })
        .then((model) => {
            return model.destroy()
        }).then(() => {
            res.send('ok')
        })
        .catch(err => {
            console.error(err);
        })
});


module.exports = router;