const config = require('./config.json');
const knex = require('knex')(config.db);
const bookshelf = require('bookshelf')(knex);

var cascadeDelete = require('bookshelf-cascade-delete');
 
bookshelf.plugin(cascadeDelete);

// inutile avec les dernières versions
//bookshelf.plugin('registry');

module.exports = bookshelf;
