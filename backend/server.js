const express = require('express');
const app = express();
var moment = require('moment');
const http = require('http').createServer(app);
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const io = require('socket.io')(http);
var Promise = require('bluebird');
const session = require('express-session')({
    secret: 'ssshhhhh',
    resave: true,
    saveUninitialized: true
});

const fileupload = require('express-fileupload');


// PORT
const PORT = process.env.PORT || 1337;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// parse application/json
app.use(bodyParser.json());
//Définition des CORS
/*app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});*/

app.use(cors());
app.use(fileupload());
app.use(express.static('public'));
app.use(express.static('public/dist'));
app.use(express.static('views'));
app.use(express.static(__dirname + '/semantic'));
app.use(session);
var sharedsession = require("express-socket.io-session");

app.use('/', express.static(path.join(__dirname, '../frontend/build')));


app.post('/uploadFile', (req, res) => {
    if (req.files) {
        console.log(req.files)
        let file = req.files.myFile,
            filename = file.name,
            path = '/public/';

        if (file.mimetype == ("video/mp4" || "video/avi")) {
            path += 'videos/'
        } else {
            path += 'images/'
        }

        file.mv(__dirname + path + filename, function (err) {
            if (err) {
                res.send(err);
            } else {
                res.send("ok");
            }
        });
    }
});

/**
 * ROUTES
 */
const route = {
    admin: require('./routes/admin.route')
};

app.use('/admin', route.admin);


/**
 * GET
 * Après route admin pour pouvoir utiliser le router dans react
 */
app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, '../frontend', 'build', 'index.html'));
});




/***
 * MODELS
 */
const Quiz = require('./models/Quiz');
const Question = require('./models/Question');
const PlayerCollection = require('./models/PlayerCollection');
const Leaderboard = require('./models/Leaderboard');
const LeaderboardCollection = require('./models/LeaderboardCollection');
const Participation = require('./models/Participation');
const ParticipationCollection = require('./models/ParticipationCollection');
const Game = require('./models/Game');
const Player = require('./models/Player');

/**
 * SOCKET.IO
 */
io.use(sharedsession(session));

var pseudo = []; // récupérer les joueurs lors de la connexion

io.on("connection", function (socket) {
    //TODO: ajouter les infos de "pseudo" en socket session

    socket.on('admin_carousel', room => {
        getQuizQuestionsReplies(room);
    });

    /**
     * PROJECTEUR
     * récupérer l'url du quiz pour l'envoyer au projecteur
     */
    socket.on('projector_room_connection', room => {
        io.sockets.emit('projector_room', room); //envoie du salon au projecteur
    });

    // lier le projecteur au salon
    socket.on('projector', room => {
        socket.join(room);
    });

    socket.on('question', data => {
        getQuestion(data.room, data.number, data.time);
    });

    socket.on('reply', (data) => {
        getReply(data.room, data.number, data.number_reply);
    });

    socket.on('play_video', res => {
        io.sockets.to(res.room).emit('play_video', res.val);
    });


    // login, initialisation de la session
    socket.on("login", (data) => {
        console.log("login")
        console.log(data)
        setPlayer(data.pseudo)
            .then(model => {
                pseudo.push({
                    socket_id: socket.id,
                    pseudo: model.attributes.pseudo,
                    pseudo_id: model.attributes.id,
                    room: data.room,
                    questions: []
                });
                socket.emit('socket_id', socket.id);
            })
            .catch(err => console.error(err))

    });


    socket.on('game_info', room => {
        socket.join(room);

        // joueurs
        getPlayers(room);

        // questions
        getNumberOfQuestions(room);

        // numéro de la question actuelle

        // classement
    });

    socket.on('number_of_questions', room => {
        getNumberOfQuestions(room);
    });

    socket.on('start_chrono', ({ room, time, number_question }) => {
        io.sockets.to(room).emit('start_chrono', time);
        // affichager sur l'écran des joueurs
        userStartAnswer(room, number_question, time);

    });
    socket.on('end_chrono', room => {
        io.sockets.to(room).emit('end_chrono', 1);
    });

    socket.on('start_quiz', data => {
        pseudo.forEach(item => {
            socket.handshake.session.cookie.userdata = {
                pseudo: item.pseudo,
                pseudo_id: item.pseudo_id,
                room: data.room,
                socket_id: socket.id,
                date: moment().format('L LTS'),
                game: Number(),
                questions: []
            };
            console.log("socket")
            console.log(socket.handshake.session.cookie.userdata)
            socket.handshake.session.cookie.originalMaxAge = 60 * 60 * 24 * 1000; // 24h
            socket.handshake.session.save();
        })
        console.log("start quiz socket")
        console.log(socket.handshake.session.cookie)
        // id => id_quiz
        startQuiz(data.room, data.id);

    });
    socket.on('end_quiz', room => {
        io.sockets.to(room).emit('end_quiz', 1);

        // suppression de tous les joueurs du salon
        pseudo.forEach(function (item, index, object) {
            if (item.room === room) {
                socket.leave(room);
                return object.splice(index, 1);
            }
        });
    });

    socket.on('projector_ranking', room => {
        io.sockets.to(room).emit('projector_show_ranking', 1);
    });

    socket.on('ranking', data => {
        classement(data.room)
            .then(collection => {
                let classement = [];
                collection.forEach(elem => {
                    classement.push({
                        id: elem.id,
                        score: elem.attributes.score,
                        player: elem.relations.player.attributes.pseudo,
                        game_id: elem.attributes.game_id
                    });
                });
                console.log("classement")
                console.log(classement)
                io.sockets.to(data.room).emit('show_ranking', classement);
            })
            .catch(err => console.error(err))
    });


    socket.on('good_reply', (data) => {
        getGoodReply(data.room, data.number);
    })
    socket.on('user_reply', (data) => {
        pseudo.forEach((item, index) => {
            if ((item.room == data.room) && (item.pseudo == data.pseudo)) {
                item.questions[data.question_number] = {
                    question_id: data.question_id,
                    question_number: data.question_number,
                    user_reply: data.user_reply
                };
                /*if (item.pseudo_id == socket.handshake.session.cookie.userdata.pseudo_id) {
                    // sauvegarde de la question avec la réponse
                    socket.handshake.session.cookie.userdata.questions = item.questions;
                    socket.handshake.session.cookie.userdata.game_id = item.game_id;
                }*/
            }
        });
    })



    socket.on("disconnect", () => {
        let room = "";
        if (pseudo.length != 0) {

            // suppression d'un joueur quand il se déconnecte
            pseudo.forEach(function (item, index, object) {
                if (item.socket_id == socket.id) {
                    room = item.room;
                    socket.leave(room);
                    return object.splice(index, 1);
                }
            });

            // envoie des nouvelles infos aux joueurs présents dans le salon
            io.sockets.to(room).emit('players', {
                number: pseudo.length,
                players: pseudo
            });
        }
    });


});
/**
 * FONCTIONS POUR SOCKET IO
 */

function initLeaderboard(room) {
    let player_leaderboard = [];

    pseudo.forEach(item => {
        player_leaderboard.push({
            score: 0,
            player_id: item.pseudo_id,
            game_id: item.game_id
        });
    })

    let learderboards = LeaderboardCollection.forge(player_leaderboard);
    return Promise.all(learderboards.invokeMap('save'));
}

function getNumberOfQuestions(room) {
    Quiz.where('url', room).fetch()
        .then(model => {
            quiz = model.serialize();

            return Question.query(function (qb) {
                qb.orderBy('order', 'ASC')
            }).where('quiz_id', quiz.id).fetchAll()
        })
        .then(collection => {
            let questions = collection.serialize();

            io.sockets.to(room).emit('number_of_questions', {
                length: questions.length,
                id: quiz.id
            });

        })
        .catch(err => console.error(err));
}

/**
 * @param {String} room 
 * @param {Number} number 
 * @param {Number} time 
 * Récupérer une question
 */
function getQuestion(room, number, time) {
    // mettre les utilisateurs en attente à chaque nouvelle question
    io.sockets.to(room).emit('users_waiting', 1);

    let quiz = {};

    Quiz.where('url', room).fetch()
        .then(model => {
            quiz = model.serialize();

            return Question.query(function (qb) {
                qb.orderBy('order', 'ASC')
            }).where('quiz_id', quiz.id).fetchAll({ withRelated: 'reply' })
        })
        .then(collection => {
            let questions = collection.serialize();
            let shuffle_replies = [];
            questions[number].reply.forEach(item => {
                shuffle_replies.push(item)
            });
            let replies = shuffle(shuffle_replies);

            io.sockets.to(room).emit('show_question', {
                question: questions[number],
                replies: replies,
                number_of_questions: questions.length,
                number_question: number + 1,
                time: time
            });
        })
        .catch(err => console.error(err));
}

function getReply(room, question_number, reply_number) {
    let quiz = {};

    Quiz.where('url', room).fetch()
        .then(model => {
            quiz = model.serialize();

            return Question.query(function (qb) {
                qb.orderBy('order', 'ASC')
            }).where('quiz_id', quiz.id).fetchAll({ withRelated: 'reply' })
        })
        .then(collection => {
            let questions = collection.serialize();
            io.sockets.to(room).emit('show_reply', {
                reply: questions[question_number].reply[reply_number],
                reply_number: reply_number
            });
        })
        .catch(err => console.error(err));
}

/**
 * Mélanger les réponses
 * @param {Array} a 
 */
function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}
function userStartAnswer(room, number, time) {
    let quiz = {};

    Quiz.where('url', room).fetch()
        .then(model => {
            quiz = model.serialize();

            return Question.query(function (qb) {
                qb.orderBy('order', 'ASC')
            }).where('quiz_id', quiz.id).fetchAll({ withRelated: 'reply' })
        })
        .then(collection => {
            let questions = collection.serialize();
            let shuffle_replies = [];
            questions[number].reply.forEach(item => {
                shuffle_replies.push(item)
            });
            let replies = shuffle(shuffle_replies);



            io.sockets.to(room).emit('user_start_answer', {
                question: questions[number],
                replies: replies,
                number_of_questions: questions.length,
                question_number: number,
                time: time
            });
        })
        .catch(err => console.error(err));
}

function getGoodReply(room, question_number) {
    //TODO: que le premier joueur qui prends les points
    let quiz = {};

    Quiz.where('url', room).fetch()
        .then(model => {
            quiz = model.serialize();

            return Question.query(function (qb) {
                qb.orderBy('order', 'ASC')
            }).where('quiz_id', quiz.id).fetchAll({ withRelated: 'reply' })
        })
        .then(collection => {
            let questions = collection.serialize();
            let good_reply = Number();

            questions[question_number].reply.forEach(element => {
                // bonne réponse
                if (element.is_correct) {
                    good_reply = Number(element.id);
                    io.sockets.to(room).emit('show_good_reply', good_reply);
                }
            });
            //TODO: faire foreach pour 1 par 1
            pseudo.forEach(item => {
                console.log("pseudo")
                console.log(item)
                let participation = {};
                if (item.room == room) {
                    participation = {
                        player_id: item.pseudo_id,
                        game_id: item.game_id,
                        question_id: item.questions[question_number].question_id,
                        date: moment().format('YYYY-MM-DD hh:mm:ss'),
                        reply_id: item.questions[question_number].user_reply
                    };
                    setScore(participation.reply_id, good_reply, participation.player_id);
                    setParticipation(participation);
                }
            });
        })
        .catch(err => console.error(err));
}
function setScore(user_reply, good_reply, player_id) {
    // ajout dans un tableau pour la table participation tous les joueurs du salon avec leurs réponses
    console.log("player reply")
    console.log(user_reply)
    console.log("good reply")
    console.log(good_reply)
    console.log(player_id)
    Leaderboard.where("player_id", player_id).fetch()
        .then(model => {
            console.log("leaderboard where")
            console.log(model)
            if (user_reply == good_reply) {
                return model.set({
                    score: Number(model.attributes.score) + 50
                }).save();
            } else {
                return null;
            }
        })
        .then(model => {
            console.log("leaderboard updated")
            console.log(model)
        })
        .catch(err => console.error(err))
}

function setParticipation(participation) {
    Participation.forge(participation).save().then(() => { return null; });
}

function startQuiz(room, id) {
    Game.forge({
        quiz_id: id,
        date: moment().format('YYYY-MM-DD hh:mm:ss')
    }).save()
        .then(model => {
            // session de jeu
            pseudo.forEach(item => {
                if (item.room == room) {
                    item.game_id = model.id;
                }
            });

            io.sockets.to(room).emit('game_id', model.id);
            return initLeaderboard(room)
        })
        .then(() => {
            console.log("start")
            io.sockets.to(room).emit('start', 1);
        })
        .catch(err => console.error(err))
}

function setPlayer(player_pseudo) {
    return Player.forge({
        pseudo: player_pseudo
    }).save();
}


function classement(room) {
    // sauvegarde de game_id dans chaque pseudo
    // fonction classement doit être appelé au moins une fois avant la fonction getGoodReply pour avoir l'attribut game_id d'enregistré
    let game_id = 0;
    pseudo.forEach((item) => {
        if (item.room == room) {
            game_id = item.game_id;
        }
    });
    return Leaderboard.where('game_id', game_id).fetchAll({ withRelated: ['player'] });
}

function getPlayers(room) {
    let players_in_room = [];
    pseudo.forEach(elem => {
        if (elem.room == room) {
            players_in_room.push(elem);
        }
    });

    io.sockets.to(room).emit('players', {
        number: players_in_room.length,
        players: players_in_room
    });
}

/**
 * LANCEMENT SERVEUR
 */
http.listen(PORT, function () {
    console.log('Serveur lancé sur le port ' + PORT);
});
