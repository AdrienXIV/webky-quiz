var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var del = require('del');
var gulp = require('gulp');
var sass = require('gulp-sass');
var terser = require('gulp-terser');
var nodemon = require('gulp-nodemon');


// Gulp task to minify CSS files
gulp.task('styles', function () {
    return gulp.src('./public/sass/*.scss')
        // Compile SASS files
        .pipe(sass({
            outputStyle: 'nested',
            precision: 10,
            includePaths: ['.'],
            onError: console.error.bind(console, 'Sass error:')
        }))
        // Auto-prefix css styles for cross browser compatibility
        .pipe(autoprefixer())
        // Minify the file
        .pipe(csso())
        // Output
        .pipe(gulp.dest('./public/dist/css'))
});

// Gulp task to minify JavaScript files
gulp.task('scripts', function () {
    return gulp.src('./public/js/**/*.js')
        // Minify the file
        .pipe(terser())
        // Output
        .pipe(gulp.dest('./public/dist/js'))
});

/*
gulp.task('minify-html', function () {
    return gulp.public(['./public/views/*.ejs', './public/views/*.html'])
        .pipe(minifyejs())
        //.pipe(rename({suffix:".min"}))
        .pipe(gulp.dest('dist'))
})
*/


// Clean output directory
gulp.task('clean', () => del(['./public/dist']));

// nodemon
gulp.task('nodemon', function (done) {
    nodemon({
        script: 'server.js',
        ext: 'js html ejs',
        env: { 'NODE_ENV': 'development' },
        done: done
    });
    done();
});

// minifier les fichiers
gulp.task('build', gulp.series('clean', 'styles', 'scripts'));

// minifier les fichiers + nodemon
gulp.task('start', gulp.series('clean', 'styles', 'scripts', 'nodemon'), function () {
    return nodemon({
        script: 'server.js',
        ext: 'js html ejs',
        env: { 'NODE_ENV': 'development' },
        done: done
    });
});

