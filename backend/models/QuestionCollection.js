const Bookshelf = require('../config/database.js');
const Question = require('./Question');
var QuestionCollection = Bookshelf.collection('Question', {
    model: Question
});

//register model and export
module.exports = QuestionCollection;