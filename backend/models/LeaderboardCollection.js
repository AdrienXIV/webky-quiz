const Bookshelf = require('../config/database.js');
const Leaderboard = require('./Leaderboard');
var LeaderboardCollection = Bookshelf.collection('Leaderboard', {
    model: Leaderboard
});

//register model and export
module.exports = LeaderboardCollection;