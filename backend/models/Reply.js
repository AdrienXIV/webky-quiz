const Bookshelf = require('../config/database.js');
var Question = require('./Question');
var Reply = Bookshelf.Model.extend({
    tableName: 'reply',
    question: function () {
        return this.belongsTo(Question);
    }
});

//register model and export
module.exports = Bookshelf.model('Reply', Reply);