const Bookshelf = require('../config/database.js');
var Player = require('./Player');
var Game = require('./Game');
var Leaderboard = Bookshelf.Model.extend({
    tableName: 'leaderboard',
    game: function () {
        return this.belongsTo(Game).through('Game');
    },
    player: function () {
        return this.belongsTo(Player);
    }
});

//register model and export
module.exports = Bookshelf.model('Leaderboard', Leaderboard);