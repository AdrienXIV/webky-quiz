const Bookshelf = require('../config/database.js');
const Player = require('./Player');
var PlayerCollection = Bookshelf.collection('Player', {
    model: Player
});

//register model and export
module.exports = PlayerCollection;