const Bookshelf = require('../config/database.js');
var Reply = require('./Reply');
var Game = require('./Game');
var Question = require('./Question');
var Player = require('./Player');
var Participation = Bookshelf.Model.extend({
    tableName: 'participation',
    game: function () {
        return this.belongsTo(Game);
    },
    question: function () {
        return this.belongsTo(Question).through('Question');
    },
    reply: function () {
        return this.belongsTo(Reply).through('Reply');
    },
    player: function () {
        return this.belongsTo(Player).through('Player');
    }
});

//register model and export
module.exports = Bookshelf.model('Participation', Participation);