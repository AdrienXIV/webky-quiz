const Bookshelf = require('../config/database.js');
var Leaderboard = require('./Leaderboard');
var Participation = require('./Participation');
var Quiz = require('./Quiz');
var Game = Bookshelf.Model.extend({
    tableName: 'game',
    leaderboard: function () {
        return this.hasMany(Leaderboard, 'game_id');
    },
    participation: function () {
        return this.hasMany(Participation, 'game_id');
    },
    quiz: function () {
        return this.belongsTo(Quiz).through('Quiz');
    },
},{
},{
    dependents: ['leaderboard', 'participation']
});

//register model and export
module.exports = Bookshelf.model('Game', Game);