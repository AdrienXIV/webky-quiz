const Bookshelf = require('../config/database.js');
var Question = require('./Question');
var Game = require('./Game');
var Quiz = Bookshelf.Model.extend({
    tableName: 'quiz',
    question: function () {
        return this.hasMany(Question, 'quiz_id');
    },
    game: function () {
        return this.hasOne(Game, 'quiz_id');
    }
});

//register model and export
module.exports = Bookshelf.model('Quiz', Quiz);