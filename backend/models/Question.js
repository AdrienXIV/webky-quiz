const Bookshelf = require('../config/database.js');
var Quiz = require('./Quiz');
var Reply = require('./Reply');
var Participation = require('./Participation');
var Question = Bookshelf.Model.extend({
    tableName: 'question',
    quiz: function () {
        return this.belongsTo(Quiz).through('Quiz');
    },
    reply: function () {
        return this.hasMany(Reply, 'question_id');
    },
    participation: function () {
        return this.belongsTo(Participation).through('Participation');
    }
}, {
    dependents: ['reply']
});

//register model and export
module.exports = Bookshelf.model('Question', Question);