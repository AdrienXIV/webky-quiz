const Bookshelf = require('../config/database.js');
const Reply = require('./Reply');
var ReplyCollection = Bookshelf.collection('Reply', {
    model: Reply
});

//register model and export
module.exports = ReplyCollection;