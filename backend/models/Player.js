const Bookshelf = require('../config/database.js');
var Learderboard = require('./Leaderboard');
var Participation = require('./Participation');
var Player = Bookshelf.Model.extend({
    tableName: 'player',
    leaderboard: function () {
        return this.hasOne(Learderboard, 'player_id');
    },
    participation: function () {
        return this.hasMany(Participation, 'player_id');
    }

}, {
    dependents: ['participation', 'leaderboard']
});

//register model and export
module.exports = Bookshelf.model('Player', Player);