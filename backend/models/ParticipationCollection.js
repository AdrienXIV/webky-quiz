const Bookshelf = require('../config/database.js');
const Participation = require('./Participation');
var ParticipationCollection = Bookshelf.collection('Participation', {
    model: Participation
});

//register model and export
module.exports = ParticipationCollection;