function deleteQuiz() {
    let r = confirm("Confirmer la suppression ?");
    if (r) {
        $.ajax({
            url: "/admin/" + $("#quiz-add-edit").data("id"),
            method: "DELETE",
            success: function () {
                window.location.href = "/admin"
            }
        });
    }
}

function addQuestion() {
    let content = "<tr>";
    content += "<td><div class='field'>";
    content += "<input class='question' type='text' name='name' value=''>";
    content += "</div></td>";
    content += "<td>";
    content += "<div class='field'>";
    content += "<span>Avant</span>";
    content += "<i class='trash alternate outline icon delete' title='supprimer image'></i>";
    content += "<input type='file' class='image_before' name='image_before' accept='image/png, image/jpeg, image/jpg, image/svg' data-path=''>";
    content += "</div>";
    content += "<div class='field'>";
    content += "<span>Après</span>";
    content += "<i class='trash alternate outline icon delete' title='supprimer image'></i>";
    content += "<input type='file' class='image_after' name='image_after' accept='image/png, image/jpeg, image/jpg, image/svg' data-path=''>";
    content += "</div>";
    content += "</td>";
    content += "<td>";
    content += "<div class='field'>";
    content += "<span>Avant</span>";
    content += "<i class='trash alternate outline icon delete' title='supprimer vidéo'></i>";
    content += "<input type='file' class='youtube_before' name='youtube_before' accept='video/mp4, video/avi' data-path=''>";
    content += "</div>";
    content += "<div class='field'>";
    content += "<span>Après</span>";
    content += "<i class='trash alternate outline icon delete' title='supprimer vidéo'></i>";
    content += "<input type='file' class='youtube_after' name='youtube_after' accept='video/mp4, video/avi' data-path=''>";
    content += "</div>";
    content += "</td>";
    content += "<td><div class='field'>";
    content += "<input class='reponses good-reply' type='text' name='name' value=''>";
    content += "</div></td>";
    content += "<td>";
    content += "<div class='field'>";
    content += "<input class='reponses bad-reply' type='text' name='name' value=''>";
    content += "</div>";
    content += "<div class='field'>";
    content += "<input class='reponses bad-reply' type='text' name='name' value=''>";
    content += "</div>";
    content += "<div class='field'>";
    content += "<input class='reponses bad-reply' type='text' name='name' value=''>";
    content += "</div>";
    content += "</td>";
    content += "<td style='text-align: center;'>";
    content += "<i title='Supprimer' class='fas fa-times delete-question'></i>";
    content += "</td>";
    content += "</tr>";

    $("#addQuestion").append(content);
};

// drag&drop
$("tbody").sortable();

/**
 * POPUP
 */
$(".ui.inverted.red.button").popup({
    content: "Supprimer le quiz"
});


$("#addQuestion").on("click", ".delete-question", function () {
    $(this).parent("td").parent("tr").remove();
});

$("#quiz-add-edit").submit(function (e) {
    e.preventDefault();

    let class_reponses = document.getElementsByClassName("reponses");
    let class_questions = document.getElementsByClassName("question");
    let image_before = document.getElementsByClassName("image_before");
    let image_after = document.getElementsByClassName("image_after");
    let youtube_before = document.getElementsByClassName("youtube_before");
    let youtube_after = document.getElementsByClassName("youtube_after");
    let table = [];
    let reponses = [];
    let compteur_reponse = 0;
    let order = 0;
    let is_correct = 0;

    for (let i = 0; i < class_reponses.length; i++) {

        compteur_reponse == 0 ? is_correct = 1 : is_correct = 0;

        reponses.push({
            name: class_reponses[i].value,
            is_correct
        });

        if (compteur_reponse == 3) {
            table.push({
                question: {
                    order,
                    name: class_questions[order].value,
                    image_before: image_before[order].getAttribute('data-path'),
                    image_after: image_after[order].getAttribute('data-path'),
                    youtube_before: youtube_before[order].getAttribute('data-path'),
                    youtube_after: youtube_after[order].getAttribute('data-path')
                },
                reponses
            });

            reponses = [];
            compteur_reponse = 0;
            order++
        } else {
            compteur_reponse++;
        }
    }

    let image = "";
    $("#quiz-image").val() == "" ? image = "https://fr.zenit.org/wp-content/uploads/2018/05/no-image-icon.png" : image = $("#quiz-image").val();

    let data = {
        name: $("#quiz-name").val(),
        description: $("#quiz-description").val(),
        image,
        table,
    };

    $.ajax({
        url: $("#quiz-add-edit").data("url"),
        method: $("#quiz-add-edit").data("method"),
        data,
        success: function (e) {
            if (e) {
                $(".ui.positive.message").fadeIn("fast").delay(1e3).fadeOut("slow");
                setTimeout(() => {
                    window.location.href = "/admin"
                }, 1500)
            }
        }
    });
});

/**
 * Envoi des images/vidéos
 * Les infos sont ajoutées dans data-path pour pouvoir les récupérer à chaque mise à jour d'un quiz
 * La suppression de ces infos entraine une suppression de l'url du média dans la BDD
 */
$("tbody").on('change', ".field input[type='file']", event => {
    if (!event.target.files[0].name.match(/.(jpg|jpeg|svg|png|gif|mp4|avi)$/i)) {
        alert('Extensions autorisées : jpg, jpeg, png, svg, gif, mp4, avi');
        return false;
    } else {
        var formData = new FormData();
        event.target.setAttribute("data-path", event.target.files[0].name);
        formData.append('myFile', event.target.files[0]);
        $.ajax({
            type: 'POST',
            url: '/uploadFile',
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            success: function (result) {
                alert("Importation réussi !")
            },
            error: function (err) {
                alert("Échec de l'importation..");
                console.log(err);
            }
        });
        alert("Importation en cours...");
    }
});
// suppression des images/vidéos
$("tbody").on("click", ".trash.alternate.outline.icon", function (e) {
    let r = confirm('Suppression de ' + e.target.parentElement.childNodes[5].getAttribute("data-path") + " ?");
    if (r) {
        e.target.parentElement.childNodes[5].setAttribute("data-path", "");
        alert("Suppression réussi.");
    }
});